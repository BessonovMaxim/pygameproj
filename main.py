import pygame

size = width, height = 1000, 500
screen = None
bg = None

def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 0)
    screen = pygame.display.set_mode(size)

def main():
    global ball_coords_x, ball_coords_y, vx, vy
    init()
    gameover = False
    ball = pygame.image.load("basketball.png")
    ball = pygame.transform.scale(ball, (100,100))
    ballrect = ball.get_rect()
    ballrect.x = 60
    ballrect.y = 50
    vx = 1
    vy = 1
    while not gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover = True
        if ballrect.x + 100> 1000:
            vx *= -1
        if ballrect.x < 0:
            vx *= -1
        if ballrect.y + 100> 500:
            vy *= -1
        if ballrect.y < 0:
            vy *= -1
        ballrect.x += vx
        ballrect.y += vy
        screen.fill(bg)
        screen.blit(ball, ballrect)
        pygame.display.flip()

main()