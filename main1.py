import pygame

size = width, height = 1000, 500
screen = None
bg = None

def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 0)
    screen = pygame.display.set_mode(size)

def main():
    global ball_coords_x, ball_coords_y, vx, vy
    init()
    gameover = False
    ball = pygame.image.load("basketball.png")
    ball = pygame.transform.scale(ball, (100,100))
    ballrect = ball.get_rect()
    ballrect.x = 60
    ballrect.y = 50
    vx = 10
    vy = 10
    while not gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover = True
        move = input()
        if move == 'w':
            ballrect.y += vy
        if move == 's':
            ballrect.y -= vy
        if move == 'a':
            ballrect.x += vx
        if move == 'd':
            ballrect.x -= vx
        screen.fill(bg)
        screen.blit(ball, ballrect)
        pygame.display.flip()

main()